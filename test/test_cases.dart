import 'package:shelf_rest/shelf_rest.dart';

class Foo {
  call(Router r) {
    r..get('/bla', () => null)..get('bar', _bar);
  }

  // Don't want to try to add this as another handler
  String _bar(String foo) => 'bar';
}

class Foo2 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Don't add as no @RestResource to tell us what is path params
  // and doesn't match naming convention
  String bar(String foo) => 'bar';
}

class Foo3 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Do add as we have the @Get
  @Get('/bar')
  String bar(String foo) => 'bar';
}

@RestResource('foo')
class Foo4 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Don't add as doesn't match naming convention
  String bar(String foo) => 'bar';
}

@RestResource('foo')
class Foo5 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Do add as does match naming convention
  String create(String foo) => 'bar';
}

@RestResource('foo')
class Foo6 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // error as flagged as a resource method but can't infor operation
  @ResourceMethod()
  String bar(String foo) => 'bar';
}

@RestResource('foo')
class Foo7 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Do add
  @ResourceMethod(operation: RestOperation.CREATE)
  String bar(String foo) => 'bar';
}

class Foo8 {
  call(Router r) {
    r..get('/bla', () => null);
  }

  // Don't add as no @RestResource to tell us what is path params
  String create(String foo) => 'bar';
}
