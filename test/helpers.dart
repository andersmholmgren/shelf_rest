library helpers;

import 'package:option/option.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_route/extend.dart';
import 'package:test/test.dart';
import 'package:uri/uri.dart';

Handler testHandler(String result) => new TestHandler(result);
OohFancy testFancyHandler(String result) => new TestFancyHandler(result);

const String breadcrumbsOut = 'breadcrumbsOut';
const String breadcrumbsIn = 'breadcrumbsIn';

Request createRequest(String path, [String method = 'GET', body]) =>
    new Request(method, Uri.parse('http://example.com/$path'), body: body);

Middleware testMiddleware(int id) {
  return (Handler innerHandler) {
    return (Request r) async {
      final response = await innerHandler(
          r.change(context: addBreadCrumb(r.context, breadcrumbsIn, id)));
      return response.change(
          context: addBreadCrumb(response.context, breadcrumbsOut, id));
    };
  };
}

Map addBreadCrumb(Map context, String key, int value) {
  final List<int> crumbs = new Map.from(context).putIfAbsent(key, () => []);
  crumbs.add(value);
  return {key: crumbs};
}

//addBreadCrumb(Request m, String key, int value) {
//  final List<int> crumbs = m.context.putIfAbsent(key, () => []);
//  crumbs.add(value);
//  return m.change(context: { key: crumbs });
//}

class TestHandler {
  final String result;

  TestHandler(this.result);

  call(Request request) {
    return new Response.ok(result,
        context: {breadcrumbsIn: request.context[breadcrumbsIn]});
  }
}

typedef Response OohFancy(Iterable<int> breadcrumbsIn);

class TestFancyHandler {
  final String result;

  TestFancyHandler(this.result);

  Response call(Iterable<int> breadcrumbs) {
    return new Response.ok(result, context: {breadcrumbsIn: breadcrumbs});
  }
}

HandlerAdapter get fancyAdapter => doFancy;

Handler doFancy(OohFancy fancy) {
  return (Request request) => fancy(request.context[breadcrumbsIn]);
}

typedef int IntMap(int i);
int intMapIdentity(int i) => i;
int plus5(int i) => i + 5;
int times5(int i) => i * 5;

TestMergeableHandlerAdapter testAdapter(IntMap mapper) =>
    new TestMergeableHandlerAdapter(mapper);

class TestMergeableHandlerAdapter extends MergeableHandlerAdapter {
  final IntMap mapper;

  TestMergeableHandlerAdapter([this.mapper = intMapIdentity]);

  @override
  HandlerAdapter merge(TestMergeableHandlerAdapter other) =>
      new TestMergeableHandlerAdapter((int i) => mapper(other.mapper(i)));

  Handler call(OohFancy fancy) => doFancy(fancy);

  Handler doFancy(OohFancy fancy) {
    return (Request request) {
      final Iterable<int> bc = request.context[breadcrumbsIn];
      final mapped = bc != null ? bc.map(mapper) : [];
      return fancy(mapped);
    };
  }
}

PathAdapter testPathAdapter(String suffix) =>
    (path) => new UriParser(new UriTemplate('$path$suffix'));

//typedef RouteableFunction RouteableAdapter(Function handler);
//typedef RouteableFunction(DefaultRouterBuilder router);
typedef TestRouteableFunction(TestRouteable router);

class TestRouteableAdapter {
  RouteableFunction call(Function handler) => adapt(handler);

  RouteableFunction adapt(TestRouteableFunction handler) {
    return (DefaultRouterBuilder builder) {
      final routeable = new TestRouteable();
      handler(routeable);

      routeable.handlers.forEach((method, h) {
        builder.add(method.toLowerCase(), [method], h);
      });

      routeable.children.forEach((path, child) {
        builder.addAll(child, path: path);
      });
    };
  }
}

class TestRouteable {
  final Map<String, Handler> handlers = {};
  final Map<String, TestRouteableFunction> children = {};

  TestRouteable();
}

DefaultSimpleRouteImpl simple(String path, Iterable<String> methods,
        bool exactMatch, String handlerResult) =>
    new DefaultSimpleRouteImpl(path, segmentRouter(path, methods, exactMatch),
        new TestHandler(handlerResult));

DefaultRouterImpl createRouter(path, Iterable<DefaultRoute> routes) =>
    new DefaultRouterImpl(
        path, segmentRouter(path, null, false), routes, const None());

typedef DefaultSegmentRouter DefaultSegmentRouterFactory();
typedef DefaultRouterBuilder DefaultRouterBuilderFactory();
typedef Request RequestFactory();
typedef RequestRouterImpl RouterFactory();
typedef SimpleRouteImpl SimpleRouteFactory();

DefaultSegmentRouter segmentRouter(
        String path, Iterable<String> methods, bool exactMatch) =>
    new DefaultSegmentRouter(
        new UriParser(new UriTemplate(path)), methods, exactMatch);

TestRouteResult testResult(
        {String result,
        Type exceptionResult,
        Iterable<String> breadcrumbsIn,
        Iterable<String> breadcrumbsOut}) =>
    new TestRouteResult(result, exceptionResult, breadcrumbsIn, breadcrumbsOut);

class TestRouteResult {
  final String result;
  final Type exceptionResult;
  final Iterable<String> breadcrumbsIn;
  final Iterable<String> breadcrumbsOut;
  TestRouteResult(this.result, this.exceptionResult, this.breadcrumbsIn,
      this.breadcrumbsOut);
}

ExpectedBehaviour behaviour(String description,
        {RequestFactory forRequest,
        TestRouteResult expectResult,
        bool expectNotHandled: false}) =>
    new ExpectedBehaviour(
        description, forRequest, expectResult, expectNotHandled);

class ExpectedBehaviour {
  final String description;
  final RequestFactory forRequest;
  final TestRouteResult expectResult;
  final bool expectNotHandled;

  ExpectedBehaviour(this.description, this.forRequest, this.expectResult,
      this.expectNotHandled);
}

void checkBehaviour(
    RouterFactory actualFactory, ExpectedBehaviour expectBehaviour) {
  group(expectBehaviour.description, () {
    RequestRouterImpl actual;

    setUp(() {
      actual = actualFactory();
    });

    if (expectBehaviour.expectNotHandled) {
      test('is not handled', () {
        expect(actual.handle(expectBehaviour.forRequest()),
            throwsA(new isInstanceOf<NotHandled>()));
      });
    } else if (expectBehaviour.expectResult.exceptionResult != null) {
      test(
          'throws http exception (${expectBehaviour.expectResult.exceptionResult})',
          () {
        expect(
            actual.handle(expectBehaviour.forRequest()),
            throwsA(predicate((e) =>
                e.runtimeType ==
                expectBehaviour.expectResult.exceptionResult)));
      });
    } else {
      test('is handled and contains response', () async {
        final actualResult = await actual.handle(expectBehaviour.forRequest());
        expect(actualResult, new isInstanceOf<Response>());
      });

      group('is handled with', () {
        final TestRouteResult expectedResult = expectBehaviour.expectResult;
        Response response;

        setUp(() async {
          response = await actual.handle(expectBehaviour.forRequest());
        });

        test('expected result', () async {
          expect(await response.readAsString(), equals(expectedResult.result));
        });

        if (expectedResult.breadcrumbsIn != null) {
          test('expected request middleware breadcrumbs', () {
            expect(response.context[breadcrumbsIn],
                orderedEquals(expectedResult.breadcrumbsIn));
          });
        }

        if (expectedResult.breadcrumbsOut != null) {
          test('expected response middleware breadcrumbs', () {
            expect(response.context[breadcrumbsOut],
                orderedEquals(expectedResult.breadcrumbsOut));
          });
        }
      });
    }
  });
}

void routerCompare(
    final RouterFactory actualFactory, final RequestRouterImpl expected) {
  RequestRouterImpl actual;
  setUp(() {
    actual = actualFactory();
  });

  test('has expected number of routes', () {
    expect(actual.routes, hasLength(expected.routes.length));
  });

  group('has segment router with', () {
    segmentRouterCompare(() => actual.segmentRouter, expected.segmentRouter);
  });

  for (int i = 0; i < expected.routes.length; i++) {
    group('then route $i', () {
      var expectedRoute = expected.routes.elementAt(i);
      var actualRouteFactory = () => actual.routes.elementAt(i);

      if (expectedRoute is SimpleRouteImpl) {
        test('is a SimpleRoute', () {
          expect(actualRouteFactory(), new isInstanceOf<SimpleRouteImpl>());
        });
        simpleRouteCompare(() => actual.routes.elementAt(i), expectedRoute);
      } else if (expectedRoute is RequestRouterImpl) {
        test('is a Router', () {
          expect(actualRouteFactory(), new isInstanceOf<RequestRouterImpl>());
        });
        routerCompare(actualRouteFactory, expectedRoute);
      } else {
        fail('unexpected type of route $expectedRoute');
      }
    });
  }
}

void simpleRouteCompare(
    final SimpleRouteFactory actualFactory, final SimpleRouteImpl expected) {
  SimpleRouteImpl actual;
  setUp(() {
    actual = actualFactory();
  });

  group('has a segment router with', () {
    segmentRouterCompare(() => actual.segmentRouter, expected.segmentRouter);
  });
}

void segmentRouterCompare(final DefaultSegmentRouterFactory actualFactory,
    final DefaultSegmentRouter expected) {
  DefaultSegmentRouter actual;

  setUp(() {
    actual = actualFactory();
  });

  test('expected path', () {
    expect(actual.path.toString(), equals(expected.path.toString()));
  });

  test('expected methods', () {
    if (expected.methods != null) {
      expect(actual.methods, unorderedEquals(expected.methods));
    } else {
      expect(actual.methods, isNull);
    }
  });
}
