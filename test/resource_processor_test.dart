library shelf_rest.resource.adapter.test;

import 'dart:convert';

import 'package:constrain/constrain.dart';
import 'package:hateoas_models/hateoas_models.dart';
import 'package:http_exception/http_exception.dart';
import 'package:option/option.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_rest/src/annotations.dart';
import 'package:shelf_rest/src/resource_processor.dart';
import 'package:shelf_rest/src/router_builder.dart';
import 'package:shelf_rest/src/router_builder_impl.dart';
import 'package:shelf_route/extend.dart'
    show DefaultRouterBuilderAdapter, RequestRouterImpl, RouteableRouterSpec;
import 'package:test/test.dart';

import 'helpers.dart';
//import 'package:shelf/shelf.dart';
//import 'package:shelf_route/src/route_impl.dart';


main() {
  group('process', () {
    group('when resource has no route information produces routerDefinition',
        () {
      RouteableRouterSpec routerDefinition;
      setUp(() {
        ShelfRestResourceProcessor processor =
            new ShelfRestResourceProcessor(new EmptyClass(), 'foo', 'foo');
        routerDefinition = processor.process();
      });

      test('which is not null', () {
        expect(routerDefinition, isNotNull);
      });

      test('that has expected path', () {
        expect(routerDefinition.path, 'foo');
      });

      test('that has no routes', () {
        expect(routerDefinition.routes, isEmpty);
      });

      test('with none routeable', () {
        expect(routerDefinition.routeable, new isInstanceOf<None>());
      });

      test('with non null routeAdapter', () {
        expect(routerDefinition.routeAdapter, isNotNull);
      });

//      test('with non null routeAdapter', () {
//        expect(routerDefinition.routeAdapter, new isInstanceOf<DefaultRouterAdapter>());
//      });

      group('with routerAdapter that adapts definition', () {
        RequestRouterImpl router;
        setUp(() {
          router = routerDefinition.routeAdapter.adapt(routerDefinition)
              as RequestRouterImpl;
        });

        test('and produces non null router', () {
          expect(router, isNotNull);
        });

        test('and produces router with no routes', () {
          expect(router.routes, isEmpty);
        });
      });
    });

    testCase('when resource has no route information',
        forResource: () => new EmptyClass(),
        expectRouter: createRouter('top', []));

    testCase('when resource has methods of type RoutableFunction',
        forResource: () => new SimpleRouteable(),
        expectRouter: createRouter('top', [
          simple('one', ['GET'], true, 'one'),
          simple('two', ['PUT'], true, 'two'),
          createRouter('subr', [
            simple('bar', ['PUT'], true, 'one')
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching full route',
              forRequest: () => createRequest('top/one'),
              expectResult: testResult(result: 'one')),
          behaviour('a request matching second route',
              forRequest: () => createRequest('top/two', 'PUT'),
              expectResult: testResult(result: '"two"'))
        ]);

    testCase('when resource has route annotations',
        forResource: () => new AddAnnotations(),
        expectRouter: createRouter('top', [
          simple('one', ['GET'], true, 'one'),
          simple('two', ['PUT'], true, 'two'),
          createRouter('subr', [
            simple('sub1', ['POST'], true, 'three')
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top/one'),
              expectResult: testResult(result: 'one', breadcrumbsIn: [1])),
          behaviour('a request matching second route',
              forRequest: () => createRequest('top/two', 'PUT'),
              expectResult: testResult(result: '"two"')),
          behaviour('a request matching third route',
              forRequest: () => createRequest('top/subr/sub1', 'POST'),
              expectResult: testResult(
                  result: 'three',
                  breadcrumbsIn: [2, 3],
                  breadcrumbsOut: [3, 2]))
        ]);

    group('when resource is missing RestResource annotation', () {
      ShelfRestResourceProcessor processor;

      setUp(() {
        processor =
            new ShelfRestResourceProcessor(new BadResource(), 'top', 'top');
      });

      test('throws', () {
        expect(() => processor.process(), throws);
      });
      test('throws an ArgumentError', () {
        expect(() => processor.process(), throwsArgumentError);
      });
    });

    testCase('when resource has rest resource annotations',
        forResource: () => new AResource(),
        expectRouter: createRouter('top', [
          simple('', ['POST'], true, 'one'),
          simple('{account}{?bar,name}', ['PUT'], true, 'two')
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top', 'POST'),
              expectResult: testResult(result: 'good', breadcrumbsIn: [1])),
          behaviour('a request matching second route',
              forRequest: () => createRequest('top/acc1?bar=3&name=fred', 'PUT',
                  JSON.encode({'accountId': '77'})),
              expectResult: testResult(result: '"blah"'))
        ]);

    testCase('when resource has inferred resource annotations',
        forResource: () => new BResource(),
        expectRouter: createRouter('top', [
          simple('{?name,age}', ['GET'], true, 'one'),
          simple('{name}{?ha}', ['DELETE'], true, 'two'),
          simple('{name}', ['GET'], true, 'two'),
          createRouter('{name}/accounts', [
            simple('', ['POST'], true, 'one'),
            simple('{account}{?bar}', ['PUT'], true, 'two')
          ]),
          createRouter('{name}/blahs', [
            simple('{blah}', ['GET'], true, 'two')
          ])
        ]),
        expectBehaviour: [
          behaviour('a request matching first route',
              forRequest: () => createRequest('top?name=fred&age=6', 'GET'),
              expectResult: testResult(result: '["hmmm"]')),
          behaviour('a request matching delete route',
              forRequest: () => createRequest('top/fred?ha=66', 'DELETE'),
              expectResult: testResult(result: '"blah"')),
          behaviour('a request matching find route',
              forRequest: () => createRequest('top/fred', 'GET'),
              expectResult: testResult(result: JSON.encode({
                "links": [
                  {"rel": "self", "href": "http://example.com/top/fred"},
                  {"rel": "delete", "href": "fred{?ha}"},
                  {"rel": "find", "href": "fred"},
                  {"rel": "accounts.foo", "href": "fred/accounts/"},
                  {
                    "rel": "accounts.blah",
                    "href": "fred/accounts/{account}{?bar}"
                  },
                  {"rel": "blahs.find", "href": "fred/blahs/{blah}"}
                ]
              }))),
          behaviour('a request matching third route',
              forRequest: () => createRequest('top/fred/accounts', 'POST'),
              expectResult: testResult(result: 'good', breadcrumbsIn: [2, 1])),
          behaviour('a request matching fourth route',
              forRequest: () => createRequest('top/barney/accounts/acc1?bar=3',
                  'PUT', JSON.encode({'accountId': '77'})),
              expectResult: testResult(result: '"blah"'))
        ]);
  });

  testCase('when resource with RoutableFunction has child annotated resource',
      forResource: () => new RouteableWithChild(),
      expectRouter: createRouter('top', [
        createRouter('', [
          createRouter('subr', [
            simple('{blah}{?name}', ['GET'], true, 'two')
          ])
        ])
      ]),
      expectBehaviour: [
        behaviour('a request matching full route',
            forRequest: () => createRequest('top/subr/xx?name=fred'),
            expectResult: testResult(result: JSON.encode({
              "links": [
                {
                  "rel": "self",
                  "href": "http://example.com/top/subr/xx?name=fred"
                },
                {"rel": "find", "href": "xx{?name}"}
              ]
            })))
      ]);

  testCase('when resource has constraints',
      forResource: () => new PersonResource(),
      expectRouter: createRouter('top', [
        simple('', ['POST'], true, 'two')
      ]),
      expectBehaviour: [
        behaviour('a request with body that satisfies constraint passes',
            forRequest: () =>
                createRequest('top', 'POST', JSON.encode({'age': 11})),
            expectResult: testResult(result: JSON.encode({"age": 11}))),
        behaviour('a request with body that violates constraint returns 400',
            forRequest: () =>
                createRequest('top', 'POST', JSON.encode({'age': 1})),
            expectResult: testResult(exceptionResult: BadRequestException))
      ]);
}

class EmptyClass {
  String foo;
  String get bar => foo;

  String blah(String s) => foo;
}

class SimpleRouteable {
  // an extra routeable to make sure only one is picked out
  blah(Router r) => createRoutes(r);

  createRoutes(Router r) {
    r
      ..get('one', testHandler('one'))
      ..put('two', () => 'two')
      ..addAll((Router r) => r..put('bar', testHandler('bar')), path: 'subr');
  }
}

Middleware _middleware1() => testMiddleware(1);
Middleware _middleware2() => testMiddleware(2);
Middleware _middleware3() => testMiddleware(3);

class AddAnnotations {
  @Get('one', middleware: _middleware1)
  Response one(Request request) => testHandler('one')(request);

  @Put('two')
  String two() => 'two';

  @AddAll(path: 'subr', middleware: _middleware2)
  AddAnnotations2 subR() => new AddAnnotations2();

  // TODO: support on getters
//  @AddAll(path: 'subr2')
//  AddAnnotations2 get subR2 => new AddAnnotations2();
}

class AddAnnotations2 {
  @Post('sub1', middleware: _middleware3)
  Response three(Request request) => testHandler('three')(request);
}

class BadResource {
  @ResourceMethod(operation: RestOperation.CREATE)
  Response foo() => new Response.ok('bad');
}

@RestResource('account')
class AResource {
  @ResourceMethod(operation: RestOperation.CREATE, middleware: _middleware1)
  Response foo(Request request) => testHandler('good')(request);

  @ResourceMethod(operation: RestOperation.UPDATE)
  String blah(String account, int bar, String name,
          @RequestBody() Account accountThing) =>
      'blah';
}

@RestResource('name')
class BResource {
  List<String> search(String name, int age) => ['hmmm'];

  String delete(String name, int ha) => 'blah';

  ResourceLinks find(
          TestCustomObject x, String name, ResourceLinksFactory factory) =>
      factory(name);

  @AddAll(path: 'accounts', middleware: _middleware2)
  AResource accounts() => new AResource();

  @AddAll(path: 'blahs', middleware: _middleware2)
  CResource blahs() => new CResource();
}

@RestResource('blah')
class CResource {
  ResourceLinks find(TestCustomObject x, String blah, String name,
          ResourceLinksFactory factory) =>
      factory(blah);
}

@RestResource('person')
class PersonResource {
  @ResourceMethod(validateParameters: true)
  Person create(@RequestBody() Person person) => person;
}

class RouteableWithChild {
  foo(Router r) {
    r
      ..addAll(
          (Router r) => r..addAll(new CResource(), path: 'subr', name: 'subr'));
  }
}

class Account {
  final String accountId;
  Account(this.accountId);
  Account.fromJson(Map json) : this(json['accountId']);
  Map toJson() => {'accountId': accountId};
}

class Person {
  @NotNull()
  @Min(10)
  final int age;

  Person(this.age);

  Person.fromJson(Map json) : this(json['age']);
  Map toJson() => {'age': age};
}

//solo_testCase(String description, {ResourceFactory forResource,
//    RequestRouterImpl expectRouter,
//    Iterable<ExpectedBehaviour> expectBehaviour}) {
//  solo_group(description, () {
//    _testCase(description, forResource, expectRouter, expectBehaviour);
//  });
//}

testCase(String description,
    {ResourceFactory forResource,
    RequestRouterImpl expectRouter,
    Iterable<ExpectedBehaviour> expectBehaviour}) {
  group(description, () {
    _testCase(description, forResource, expectRouter, expectBehaviour);
  });
}

_testCase(
    String description,
    ResourceFactory forResource,
    RequestRouterImpl expectRouter,
    Iterable<ExpectedBehaviour> expectBehaviour) {
//  factory ShelfRestRouterBuilder(path, {String name,
//  r.RouterAdapter routeAdapter, routeable}) = ShelfRestRouterBuilderImpl;

  Router builderFactory() => new ShelfRestRouterBuilder.def('',
      routeAdapter: new DefaultRouterBuilderAdapter.def(
          handlerAdapter: handlerAdapter(customObjects: {
        TestCustomObject: (Request r) => new TestCustomObject()
      })))..addAll(forResource(), path: 'top', name: 'top');

//  ShelfRestResourceProcessor processorFactory() =>
//      new ShelfRestResourceProcessor(forResource(), 'top', 'top');
//  RouterSpec routerDefinitionFactory() => processorFactory().process();
  RequestRouterImpl actualFactory() =>
      builderFactory().build().routes.first as RequestRouterImpl;

  if (expectRouter != null) {
    routerCompare(actualFactory, expectRouter);
  }

  if (expectBehaviour != null) {
    group('then', () {
      for (var expected in expectBehaviour) {
        checkBehaviour(actualFactory, expected);
      }
    });
  }
}

typedef ResourceFactory();

class TestCustomObject {}
