// Copyright (c) 2015, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.router.builder.impl;

import 'package:option/option.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_rest/src/resource_processor.dart';
import 'package:shelf_rest/src/router_builder.dart';
import 'package:shelf_route/extend.dart' as r;

class ShelfRestRouterBuilder<B extends ShelfRestRouterBuilder>
    extends r.ShelfRouteRouterBuilder<B> implements Router<B> {
  ShelfRestRouterBuilder(Function fallbackHandler, String name, path,
      r.RouterAdapter routerAdapter, routeable, Middleware middleware)
      : super(
            fallbackHandler, name, path, routerAdapter, routeable, middleware);

  ShelfRestRouterBuilder.def(path,
      {String name,
      Function fallbackHandler,
      r.RouterAdapter routeAdapter,
      routeable,
      Middleware middleware})
      : super(fallbackHandler, name, (path != null ? path : '/'), routeAdapter,
            routeable, middleware);

  ShelfRestRouterBuilder.create(
      {String name,
      Function fallbackHandler,
      r.HandlerAdapter handlerAdapter,
      r.RouteableAdapter routeableAdapter,
      r.PathAdapter pathAdapter,
      Middleware middleware,
      path,
      routeable})
      : super.create(
            name: name,
            fallbackHandler: fallbackHandler,
            handlerAdapter: handlerAdapter,
            routeableAdapter: routeableAdapter,
            pathAdapter: pathAdapter,
            middleware: middleware,
            path: path,
            routeable: routeable);

  void addAll(routeable,
      {dynamic path,
      r.RouteableAdapter routeableAdapter,
      r.PathAdapter pathAdapter,
      r.HandlerAdapter handlerAdapter,
      Middleware middleware,
      String name}) {
    if (routeable is r.RouteableFunction) {
      super.addAll(routeable,
          path: path,
          routeableAdapter: routeableAdapter,
          pathAdapter: pathAdapter,
          handlerAdapter: handlerAdapter,
          middleware: middleware,
          name: name);
    } else {
      final processor = new ShelfRestResourceProcessor(routeable, name, path,
          routerAdapter: new r.DefaultRouterBuilderAdapter(
              handlerAdapter, pathAdapter, routeableAdapter),
          middleware: middleware);

      // As we support arbitrary RouteableFunctions on an resource in the
      // hierarchy, we need to create child ShelfRestRouterBuilderImpl instances
      // as we go. So we can't simply call addRouter(processor.process());
      final routerSpec = processor.process();
      _addRouterSpec(routerSpec);
    }
  }

  void _addRouterSpec(r.RouteableRouterSpec routerSpec) {
    final ra = routerSpec.routeAdapter as r.BaseRouteAdapter;

    addAll((ShelfRestRouterBuilder b) {
      if (routerSpec.routeable is Some) {
        routerSpec.routeable.get()(b);
      }

      routerSpec.routes
          .where((route) => route is r.SimpleRouteSpec)
          .forEach((route) {
        b.addRoute(route);
      });

      routerSpec.routes
          .where((route) => route is r.RouterSpec)
          .forEach((route) {
        b._addRouterSpec(route);
      });
    },
        handlerAdapter: ra.handlerAdapter,
        pathAdapter: ra.pathAdapter,
        middleware: routerSpec.middleware,
        name: routerSpec.name,
        path: routerSpec.path);
  }

  void resource(routeable,
      {dynamic path,
      r.RouteableAdapter routeableAdapter,
      r.PathAdapter pathAdapter,
      r.HandlerAdapter handlerAdapter,
      Middleware middleware,
      String name}) {
    addAll(routeable,
        path: path,
        routeableAdapter: routeableAdapter,
        pathAdapter: pathAdapter,
        handlerAdapter: handlerAdapter,
        middleware: middleware,
        name: name);
  }

  ShelfRestRouterBuilder<B> createChild(String name, path, routeable,
          r.RouterAdapter routerAdapter, Middleware middleware) =>
      new ShelfRestRouterBuilder(
          fallbackHandler, name, path, routerAdapter, routeable, middleware);
}
