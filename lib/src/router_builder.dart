// Copyright (c) 2015, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.router.builder;

import 'package:shelf/shelf.dart';
import 'package:shelf_route/shelf_route.dart' as r;

/// A router builder for shelf_rest
abstract class Router<B extends Router> extends r.Router<B> {
  /// add a shelf_rest REST resource
  /// Note this is just an alias for [addAll]
  void resource(routeable,
      {dynamic path,
      r.RouteableAdapter routeableAdapter,
      r.PathAdapter pathAdapter,
      r.HandlerAdapter handlerAdapter,
      Middleware middleware,
      String name});
}
