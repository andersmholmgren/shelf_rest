// Copyright (c) 2015, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.processor;

import 'dart:mirrors';

import 'package:hateoas_models/hateoas_models.dart';
import 'package:option/option.dart';
import 'package:quiver/iterables.dart';
import 'package:shelf/shelf.dart';
import 'package:shelf_bind/shelf_bind.dart' as bind;
import 'package:shelf_rest/src/adapters.dart';
import 'package:shelf_rest/src/annotations.dart';
import 'package:shelf_rest/src/util.dart';
import 'package:shelf_route/extend.dart';

export 'package:shelf_bind/shelf_bind.dart';

/// Produces routers from Dart classes for shelf_rest by using reflection
class ShelfRestResourceProcessor extends BaseResourceProcessor {
  Option<RestResource> _restResourceOpt = const None();
  Option<RestResource> get restResourceOpt => _restResourceOpt;
  bool get isRestResourceHandlingEnabled => _restResourceOpt is Some;
  Option<String> get pathParameterNameOpt =>
      restResourceOpt.map((rr) => rr.pathParameterName);

  final Set<RestOperation> _handledOperations = new Set();
  final String routeName;
  final path;
  final Option<ShelfRestResourceProcessor> parentProcessor;
  final Option<RouterAdapter> routerAdapter;
  final Option<Middleware> middleware;

  Option<RouteableFunction> _providedRouteable = const None();

  Iterable<String> get pathParameters {
    final parentParams =
        parentProcessor.map((pp) => pp.pathParameters).getOrElse(() => []);
    final ourPathParam = pathParameterNameOpt
        .map((pathParameterName) => [pathParameterName])
        .getOrElse(() => []);
    return concat([parentParams, ourPathParam]);
  }

  ShelfRestResourceProcessor(resource, this.routeName, this.path,
      {ShelfRestResourceProcessor parentProcessor,
      RouterAdapter routerAdapter,
      Middleware middleware})
      : this.parentProcessor = new Option(parentProcessor),
        this.routerAdapter = new Option(routerAdapter),
        this.middleware = new Option(middleware),
        super.from(resource);

  void processResourceAnnotations(List<InstanceMirror> metadata) {
    _restResourceOpt = getAnnotationFrom(metadata, RestResource);
  }

  Option<RouteSpec> processMethodClosure(
      Symbol name, ClosureMirror closureMirror) {
    if (closureMirror.reflectee is RouteableFunction) {
      if (_providedRouteable is None) {
        _providedRouteable = new Some(closureMirror.reflectee);
      }
      return const None();
    }

    final closureProcessors = [
      _processAddAnnotation,
      _processAddAllAnnotation,
      _processResourceMethodAnnotation,
      _processResourceMethodByNamingConvention
    ];

    return closureProcessors
        .map((cp) => cp(name, closureMirror))
        .firstWhere((o) => o is Some, orElse: () => const None());
  }

  Option<SimpleRouteSpec> _processAddAnnotation(
      Symbol name, ClosureMirror closureMirror) {
    return getAnnotation(closureMirror.function, Add).map((Add add) =>
        new SimpleRouteSpec(
            MirrorSystem.getName(name),
            add.path,
            add.methods,
            add.exactMatch,
            closureMirror.reflectee,
            new DefaultSimpleRouteAdapter(add.handlerAdapter, add.pathAdapter),
            add.middleware));
  }

  Option<RouterSpec> _processAddAllAnnotation(
      Symbol name, ClosureMirror closureMirror) {
    return getAnnotation(closureMirror.function, AddAll).map((AddAll addAll) {
      var instanceMirror = closureMirror.apply([]);

      final pathPrefix =
          pathParameterNameOpt.map((ppn) => '{$ppn}/').getOrElse(() => '');
      final childPath = pathPrefix + addAll.path;

      return new ShelfRestResourceProcessor(
              // TODO: a waste to not pass in instanceMirror
              instanceMirror.reflectee,
              MirrorSystem.getName(name),
              childPath,
              parentProcessor: this,
              routerAdapter: new DefaultRouterBuilderAdapter(
                  addAll.handlerAdapter,
                  addAll.pathAdapter,
                  addAll.routeableAdapter),
              middleware: addAll.middleware)
          .process();
    });
  }

  Option<SimpleRouteSpec> _processResourceMethodAnnotation(
      Symbol name, ClosureMirror closureMirror) {
    return getAnnotation(closureMirror.function, ResourceMethod).expand(
        (ResourceMethod resourceMethod) => _processResourceMethod(
            name, new Some(resourceMethod), closureMirror));
  }

  // same as _processResourceMethodAnnotation but inferring operation
  Option<SimpleRouteSpec> _processResourceMethodByNamingConvention(
      Symbol name, ClosureMirror closureMirror) {
    return _processResourceMethod(name, const None(), closureMirror);
  }

  Option<SimpleRouteSpec> _processResourceMethod(
      Symbol name,
      Option<ResourceMethod> providedResourceMethod,
      ClosureMirror closureMirror) {
    final Option<ResourceMethod> inferredResourceMethod =
        _inferResourceMethod(name);

    final Option<ResourceMethod> resourceMethodOpt =
        _mergeResourceMethod(providedResourceMethod, inferredResourceMethod);

    return resourceMethodOpt.map((ResourceMethod resourceMethod) {
      if (restResourceOpt is None) {
        throw new ArgumentError(
            'Can only use @ResourceMethod annotations on classes annotated with '
            '@RestResource');
      }

      final operation = resourceMethod.operation;
      if (_handledOperations.contains(operation)) {
        throw new ArgumentError(
            'Found more than one method that maps to rest operation $operation');
      }
      _handledOperations.add(operation);

      return new SimpleRouteSpec(
          MirrorSystem.getName(name),
          null,
          [resourceMethod.httpMethod],
          true,
          closureMirror.reflectee,
          new ShelfRestSimpleRouteAdapter(() => pathParameters, resourceMethod),
          resourceMethod.middleware);
    });
  }

  Option<ResourceMethod> _inferResourceMethod(Symbol name) {
    return new Option(RestOperation
            .defaultOperationForMethodName(MirrorSystem.getName(name)))
        .map((operation) => new ResourceMethod(operation: operation));
  }

  @override
  RouteableRouterSpec createRouter(Iterable<RouteSpec> routes) {
    final ra = routerAdapter
        .getOrElse(() => new DefaultRouterBuilderAdapter.def())
        .merge(new DefaultRouterAdapter.def(handlerAdapter: bind.handlerAdapter(
            customObjects: {
              ResourceLinksFactory: (Request r) => _createResourceLinks(r)
            })));

    return new RouteableRouterSpec(routeName, path, routes, ra,
        _providedRouteable, middleware.getOrElse(() => null));
  }

  ResourceLinksFactory _createResourceLinks(Request request) {
    final builder = new ResourceLinksBuilder();

    return new DefaultResourceLinksFactory((resourceId, {bool includeSelf}) {
      final routingContextOpt = getRoutingContext(request);
      if (routingContextOpt is None) {
        throw new StateError('must be called during routing');
      }

      final RoutingContext rc = routingContextOpt.get();

      final DefaultRouter containingRouter = rc.containingRouter;

      final pathParam =
          pathParameterNameOpt.map((ppn) => '{$ppn}').getOrElse(() => '');

      // TODO: make sure the validation still works

      if (includeSelf) {
        builder.addSelf(request.requestedUri);
      }

      // we include routes that are for a specific instance of a resource
      // (find, update, delete etc) at
      // the top level and for any immediate child routers we include the
      // routes that operate at the collection level like search and create

      containingRouter.routes
          .where((r) =>
              r is DefaultSimpleRoute &&
              r.segmentRouter.path.toString().startsWith(pathParam))
          .forEach((simpleRoute) {
        final String rawPath = simpleRoute.segmentRouter.path.toString();
        final path = rawPath.replaceFirst(pathParam, resourceId);
        builder.add(simpleRoute.name, path);
      });

      containingRouter.routes
          .where((r) => r is DefaultRouter)
          .forEach((router) {
        final String rawPath = router.segmentRouter.path.toString();
        final String pp = pathParam;
        final path = rawPath.startsWith(pp)
            ? rawPath.replaceFirst(pp, resourceId)
            : rawPath;
        _addResourceCollectionLinks(builder, router, path);
      });

      return builder.build();
    });
  }

  void _addResourceCollectionLinks(
      ResourceLinksBuilder builder, DefaultRouter router, String path) {
    router.routes.where((r) => r is DefaultSimpleRoute).forEach((simpleRoute) {
      builder.child(
          router.name,
          (ResourceLinksBuilder cb) => cb.add(
              simpleRoute.name, '$path/${simpleRoute.segmentRouter.path}'));
    });
  }
}

Option<ResourceMethod> _mergeResourceMethod(
    Option<ResourceMethod> providedResourceMethod,
    Option<ResourceMethod> inferredResourceMethod) {
  if (providedResourceMethod is Some && inferredResourceMethod is Some) {
    return new Some(
        providedResourceMethod.get().merge(inferredResourceMethod.get()));
  } else if (providedResourceMethod is Some) {
    return providedResourceMethod;
  } else if (inferredResourceMethod is Some) {
    return inferredResourceMethod;
  } else {
    return const None();
  }
}

/// Base class for processors that create [RouterDefinition]s from classes
/// by using mirrors.
abstract class BaseResourceProcessor {
  final InstanceMirror resourceMirror;

  BaseResourceProcessor(this.resourceMirror);
  BaseResourceProcessor.from(resource) : this(reflect(resource));

  RouterSpec process() {
    processResourceAnnotations(resourceMirror.type.metadata);
    final methodRoutes = processMethods(getMethodMirrors(resourceMirror));
    // TODO: add support for getters
//    final getterRoutes = processGetters(getGetters(resourceMirror));
    return createRouter(methodRoutes);
  }

  RouterSpec createRouter(Iterable<RouteSpec> routes);

  Iterable<RouteSpec> processMethods(Map<Symbol, MethodMirror> methodMirrors) {
    final routes = [];
    methodMirrors.forEach((k, mm) {
      if (!mm.isPrivate) {
        final routeOpt = processMethod(k, mm);
        if (routeOpt is Some) {
          routes.add(routeOpt.get());
        }
      }
    });

    return routes;
  }

  void processResourceAnnotations(List<InstanceMirror> metadata) {}

  Option<RouteSpec> processMethod(Symbol name, MethodMirror methodMirror) =>
      processMethodClosure(name, resourceMirror.getField(name));

  Option<RouteSpec> processMethodClosure(
      Symbol name, ClosureMirror closureMirror);
}
