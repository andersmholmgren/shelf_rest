library shelf_bind.util;

import 'dart:mirrors';

import 'package:option/option.dart';

Iterable<Map> iterableToJson(Iterable i) =>
    i == null ? null : i.map((j) => j.toJson()).toList(growable: false);

Option<dynamic> getAnnotation(DeclarationMirror dm, Type annotationType) =>
    getAnnotationFrom(dm.metadata, annotationType);

Option<dynamic> getAnnotationFrom(
    List<InstanceMirror> metadata, Type annotationType) {
  final annotations = metadata
      .where((md) => reflectType(annotationType).isAssignableTo(md.type));
  return annotations.isEmpty
      ? const None()
      : new Some(metadata.first.reflectee);
}

MethodMirror getMethodMirror(InstanceMirror resource, String methodName) {
  final dm = resource.type.declarations[new Symbol(methodName)];
  if (dm == null) {
    return null;
  }
  if (dm is MethodMirror && dm.isRegularMethod) {
    return dm;
  }

  return null;
}

Map<Symbol, MethodMirror> getMethodMirrors(InstanceMirror resource,
    {bool includeGetters: false}) {
  final methods = <Symbol, MethodMirror>{};

  resource.type.declarations.forEach((k, v) {
    if (v is MethodMirror &&
        (v.isRegularMethod || (includeGetters && v.isGetter))) {
      methods[k] = v;
    }
  });

  return methods;
}

//Map<Symbol, MethodMirror> getMethodMirrors(InstanceMirror resource) {
