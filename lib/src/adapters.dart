// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

library shelf_rest.resource.adapter;

import 'package:shelf/shelf.dart';
import 'package:shelf_bind/shelf_bind.dart';
import 'package:shelf_rest/src/annotations.dart';
import 'package:shelf_route/extend.dart';

export 'package:shelf_bind/shelf_bind.dart';

typedef Iterable<String> PathParameterProvider();

/// A custom [SimpleRouteAdapter] to handle methods that have [ResourceMethod]
/// annotations or match the naming scheme that infers [ResourceMethod]
/// annotations.
/// This is a custom adapter because it needs to generate the path expressions
/// based on a number of factors including what path parameters are defined on
/// this and parent resources; what custom objects are included and inherited
/// by the corresponding [HandlerAdapter] etc.
/// This is easiest done during the adaption process
class ShelfRestSimpleRouteAdapter extends BaseRouteAdapter<DefaultSimpleRoute>
    implements SimpleRouteAdapter {
  final PathParameterProvider pathParameterProvider;
  final bool isCollectionOperation;

  BoundHandlerAdapter get boundHandlerAdapter =>
      this.handlerAdapter as BoundHandlerAdapter;

  ShelfRestSimpleRouteAdapter(
      this.pathParameterProvider, ResourceMethod resourceMethod)
      : this.isCollectionOperation =
            resourceMethod.operation.isCollectionOperation,
        super(
            new BoundHandlerAdapter(
                validateParameters: resourceMethod.validateParameters,
                validateReturn: resourceMethod.validateReturn),
            uriTemplatePattern);

  ShelfRestSimpleRouteAdapter._(
      this.pathParameterProvider,
      this.isCollectionOperation,
      HandlerAdapter handlerAdapter,
      PathAdapter pathAdapter)
      : super(handlerAdapter, pathAdapter);

  DefaultSimpleRoute adapt(SimpleRouteSpec route) {
    final handlerBinding = boundHandlerAdapter.handlerBinding(route.handler);

    final Iterable<String> allPathParameters = pathParameterProvider();
    final parentPathParameters =
        allPathParameters.take(allPathParameters.length - 1);
    final pathParameter = allPathParameters.last;

    final excludedPathParameters =
        isCollectionOperation ? parentPathParameters : allPathParameters;

    bool bindingFilter(HandlerParameterBinding binding) {
      return binding is BasicHandlerParameterBinding &&
          binding.source is PathHandlerParameterSource &&
          !excludedPathParameters.contains(binding.handlerParameterName);
    }

    final queryParams = handlerBinding.parameterBindings
        .where(bindingFilter)
        .map((binding) => binding.handlerParameterName);

    final mainPath = !isCollectionOperation ? '{${pathParameter}}' : '';
    final query = queryParams.isNotEmpty ? '{?${queryParams.join(",")}}' : '';
    final path = '$mainPath$query';

    return new DefaultSimpleRouteImpl(
        route.name,
        new DefaultSegmentRouter(
            pathAdapter(path), route.methods, route.exactMatch),
        _adaptHandler(route.handler, route.middleware));
  }

  // TODO: factor out to share with DefaultSimpleRouteAdapter
  Handler _adaptHandler(handler, Middleware middleware) {
    final adapted = this.handlerAdapter(handler);
    return middleware != null
        ? new Pipeline().addMiddleware(middleware).addHandler(adapted)
        : adapted;
  }

  @override
  ShelfRestSimpleRouteAdapter create(HandlerAdapter mergedHandlerAdapter,
      PathAdapter mergedPathAdapter, DefaultRouterAdapter other) {
    return new ShelfRestSimpleRouteAdapter._(pathParameterProvider,
        isCollectionOperation, mergedHandlerAdapter, mergedPathAdapter);
  }
}
