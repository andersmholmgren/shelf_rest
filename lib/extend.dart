// Copyright (c) 2015, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

/// The shelf_rest.extend library.
///
/// `shelf_rest` package is an extension of
/// [shelf_route](https://pub.dartlang.org/packages/shelf_route), which is
/// itself also extensible.
///
/// For example [mojito](https://pub.dartlang.org/packages/mojito) extends
/// the `shelf_rest` Router to add several helper methods.
///
/// Consult the [shelf_route](https://pub.dartlang.org/packages/shelf_route)
/// documentation for more information
///

library shelf_rest.extend;

export 'package:shelf_rest/src/router_builder_impl.dart';

export 'shelf_rest.dart';
