// Copyright (c) 2015, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

/// The shelf_rest library.
///
/// The shelf_rest library is a drop in replacement of
/// [shelf_route](https://pub.dartlang.org/packages/shelf_route) Router with the
/// following additions:
///
/// 1. The [shelf_bind](https://pub.dartlang.org/packages/shelf_bind)
/// [HandlerAdapter] is automatically installed so you can use ordinary Dart
/// functions as handlers
/// 1. The [shelf_bind](https://pub.dartlang.org/packages/shelf_bind)
/// annotations such as [RequestBody] are automatically
/// available
/// 1. You can use annotations such as [Get] and [Post] in addition to the
/// corresponding `get` and `post` methods on [Router]. These annotations are
/// just another way of configuring the same thing and you can mix and match
/// the two styles as you wish
/// 1. You can use the [RestResource] and [ResourceMethod] annotations to
/// create your routes in a higher level, consistent manner
/// 1. You can add an argument to your handler methods of type
/// [ResourceLinksFactory] to get a factory to help create
/// [HATEOAS](http://en.wikipedia.org/wiki/HATEOAS) links.
///
library shelf_rest;

export 'package:hateoas_models/hateoas_models.dart';
export 'package:shelf_bind/shelf_bind.dart';
export 'package:shelf_rest/src/annotations.dart';
export 'package:shelf_rest/src/api.dart';
export 'package:shelf_rest/src/router_builder.dart';
export 'package:shelf_route/shelf_route.dart' hide Router, router;
