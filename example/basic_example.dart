// Copyright (c) 2014, The Shelf REST project authors.
// Please see the AUTHORS file for details.
// All rights reserved. Use of this source code is governed by
// a BSD 2-Clause License that can be found in the LICENSE file.

import 'package:shelf/shelf.dart';
import 'package:shelf/shelf_io.dart' as io;
import 'package:shelf_rest/shelf_rest.dart';

void main() {
  var rootRouter = router()..addAll(new AccountResource(), path: 'accounts');

  var handler = const Pipeline()
      .addMiddleware(logRequests())
      .addHandler(rootRouter.handler);

  printRoutes(rootRouter);

  io.serve(handler, 'localhost', 8080).then((server) {
    print('Serving at http://${server.address.host}:${server.port}');
  });
}

class Account {
  final String accountId;
  final String name;

  Account.build({this.accountId, this.name});

  Account.fromJson(Map json)
      : this.accountId = json['accountId'],
        this.name = json['name'];

  Map toJson() => {'accountId': accountId, 'name': name};
}

@RestResource('accountId')
class AccountResource {
  List<Account> search(String name) {
    return [new Account.build(accountId: 'A999', name: name)];
  }

  Account create(Account account) => account;

  Account update(@RequestBody() Account account) => account;

  @ResourceMethod(middleware: logRequests)
  Account find(String accountId) => new Account.build(accountId: accountId);

  @AddAll(path: 'deposits', middleware: logRequests)
  DepositResource deposits() => new DepositResource();
}

class Deposit {
  double amount;
  final String depositId;

  Deposit.build({this.depositId});

  Deposit.fromJson(Map json)
      : this.depositId = json['depositId'],
        this.amount = json['amount'];

  Map toJson() => {'depositId': depositId, 'amount': amount};
}

@RestResource('depositId')
class DepositResource {
  @ResourceMethod(method: 'PUT')
  Deposit create(Deposit deposit) => deposit;
}
